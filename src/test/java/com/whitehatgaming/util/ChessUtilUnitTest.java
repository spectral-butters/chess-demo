package com.whitehatgaming.util;

import com.whitehatgaming.entity.Move;
import com.whitehatgaming.exceptions.InvalidMoveException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class ChessUtilUnitTest {

    @Test
    public void testCreateMove() {
        // Setup
        final Move expectedResult = new Move(new int[][]{{0, 1}}, new int[][]{{2, 4}});

        // Run the test
        final Move result = ChessUtil.createMove("a2c5");

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testCreateMove_InvalidInput() {
        // Run the test
        assertThrows(InvalidMoveException.class, () -> ChessUtil.createMove("move"));
        assertThrows(InvalidMoveException.class, () -> ChessUtil.createMove("a0b0"));
        assertThrows(InvalidMoveException.class, () -> ChessUtil.createMove("aa0"));
        assertThrows(InvalidMoveException.class, () -> ChessUtil.createMove("a2b43"));
        assertThrows(InvalidMoveException.class, () -> ChessUtil.createMove("a9b9"));
        assertThrows(InvalidMoveException.class, () -> ChessUtil.createMove("-2b3"));
        assertThrows(InvalidMoveException.class, () -> ChessUtil.createMove("a1a1"));

        // Min and max allowed coordinates
        ChessUtil.createMove("a1h8");

    }
}
