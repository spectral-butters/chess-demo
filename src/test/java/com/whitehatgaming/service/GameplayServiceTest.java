package com.whitehatgaming.service;

import com.whitehatgaming.entity.*;
import com.whitehatgaming.entity.figures.King;
import com.whitehatgaming.entity.figures.Pawn;
import com.whitehatgaming.entity.figures.Queen;
import com.whitehatgaming.entity.figures.Rook;
import com.whitehatgaming.exceptions.CheckException;
import com.whitehatgaming.exceptions.InvalidMoveException;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class GameplayServiceTest {

    GameplayService gameplayService;

    @BeforeEach
    void setUp() {
        gameplayService = new GameplayService(mock(ApplicationShutdown.class));
    }


    @Test
    void printBoard() {
        Board board = new Board();
        board.initialize();
        board.printBoard();
    }

    @Test
    void validMove_MoveKing_UpdatePlayerCells() {
        Game game = new Game();
        game.initialize();
        // Arrange
        int[][] from = new int[][]{{4, 0}};
        int[][] to = new int[][]{{4, 1}};
        // Remove pawn from the way
        Player currentPlayer = game.getCurrentPlayer();
        game.getBoard().getCell(to).setFigure(null);
        currentPlayer.removeOccupiedCell(game.getBoard().getCell(to));

        String move = "e1e2";
        Cell fromCell = game.getBoard().getCell(from);
        Cell toCell = game.getBoard().getCell(to);
        assertTrue(currentPlayer.getOccupiedCells().contains(fromCell));
        assertFalse(currentPlayer.getOccupiedCells().contains(toCell));

        // Act
        gameplayService.move(move, game);

        // Assert
        assertNull(fromCell.getFigure());
        assertEquals(Type.K, toCell.getFigure().getType());
        assertTrue(currentPlayer.getOccupiedCells().contains(toCell));
        assertFalse(currentPlayer.getOccupiedCells().contains(fromCell));
        assertEquals(toCell, currentPlayer.getKingCell());
    }

    @Test
    void validMove_EatFigure_UpdatePlayerCells() {
        Game game = new Game();
        game.initialize();
        // Arrange
        int[][] from = new int[][]{{4, 1}};
        int[][] to = new int[][]{{5, 2}};
        String move = "e2f3";
        // Put opponent figure in the way
        Player currentPlayer = game.getCurrentPlayer();
        Player opponent = game.getCurrentOpponent();
        game.getBoard().getCell(to).setFigure(new Pawn(Color.BLACK));
        opponent.addOccupiedCell(game.getBoard().getCell(to));

        Cell fromCell = game.getBoard().getCell(from);
        Cell toCell = game.getBoard().getCell(to);

        assertTrue(currentPlayer.getOccupiedCells().contains(fromCell));
        assertFalse(currentPlayer.getOccupiedCells().contains(toCell));
        assertTrue(opponent.getOccupiedCells().contains(toCell));

        // Eat opponent's figure
        gameplayService.move(move, game);

        // Assert
        assertNull(fromCell.getFigure());
        assertFalse(currentPlayer.getOccupiedCells().contains(fromCell));
        assertTrue(currentPlayer.getOccupiedCells().contains(toCell));
        assertFalse(opponent.getOccupiedCells().contains(toCell));
    }

    @Test
    void invalidMove_EatKing_Error() {
        Game game = new Game();
        game.initialize();
        // Arrange
        int[][] from = new int[][]{{4, 1}};
        int[][] to = new int[][]{{5, 2}};
        String move = "e2f3";
        // Put opponent King figure in the board
        Player currentPlayer = game.getCurrentPlayer();
        Player opponent = game.getCurrentOpponent();
        game.getBoard().getCell(to).setFigure(new King(Color.BLACK));
        opponent.addOccupiedCell(game.getBoard().getCell(to));

        // Eat opponent's King
        assertThrows(InvalidMoveException.class, () -> gameplayService.move(move, game));
    }

    @Test
    void invalidMove_underCheck() {
        Game game = new Game();
        game.initialize();
        // Arrange
        // Enemy Queen on a5
        int[][] enemy = new int[][]{{0, 4}};
        // Trying to move pawn from d2
        int[][] from = new int[][]{{3, 1}};
        int[][] to = new int[][]{{3, 2}};
        String move = "d2d3";
        // Put opponent figure on the board
        game.getBoard().getCell(enemy).setFigure(new Queen(Color.BLACK));
        game.getCurrentOpponent().addOccupiedCell(game.getBoard().getCell(enemy));

        // Check exception must be thrown
        assertThrows(CheckException.class, () -> gameplayService.move(move, game));
    }

    @Test
    void movePawn() {
        Game game = new Game();
        game.initialize();

        // Test one step
        int[][] from = new int[][]{{0, 1}};
        int[][] to = new int[][]{{0, 2}};

        assertNull(game.getBoard().getCell(to).getFigure());

        String move = "a2a3";
        gameplayService.move(move, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.P, game.getBoard().getCell(to).getFigure().getType());

        game.initialize();

        //Test 2 steps
        from = new int[][]{{0, 1}};
        to = new int[][]{{0, 3}};
        move = "a2a4";

        gameplayService.move(move, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.P, game.getBoard().getCell(to).getFigure().getType());

        game.initialize();

        // Eat figure
        from = new int[][]{{0, 1}};
        to = new int[][]{{1, 2}};
        game.getBoard().getCell(to).setFigure(new Pawn(Color.BLACK));
        move = "a2b3";

        gameplayService.move(move, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.P, game.getBoard().getCell(to).getFigure().getType());
        assertEquals(Color.WHITE, game.getBoard().getCell(to).getFigure().getColor());

        // Eat figure fail
        to = new int[][]{{1, 3}};
        game.getBoard().getCell(to).setFigure(new Pawn(Color.BLACK));
        String failEatMove = "a2b4";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(failEatMove, game));


        //Test failed move
        from = new int[][]{{0, 1}};
        to = new int[][]{{0, 4}};
        String failMove = "a2a5";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(failMove, game));

    }

    @Test
    void moveRook() {
        Game game = new Game();
        game.initialize();

        // Move the rook horizontally
        int[][] from = new int[][]{{0, 0}};
        int[][] to = new int[][]{{0, 3}};
        // Remove the pawn from the way
        game.getBoard().getCell(new int[][]{{0, 1}}).setFigure(null);
        String move = "a1a4";

        gameplayService.move(move, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.R, game.getBoard().getCell(to).getFigure().getType());

        // Test vertically from last position
        // Move the rook vertically
        from = new int[][]{{0, 3}};
        to = new int[][]{{3, 3}};
        move = "a4d4";

        gameplayService.move(move, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.R, game.getBoard().getCell(to).getFigure().getType());

        // Test invalid move (diagonal movement)
        from = new int[][]{{3, 3}};
        to = new int[][]{{4, 4}};
        String invalidMove = "d4e5";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(invalidMove, game));

        // Assert it doesn't go through obstacles
        game.getBoard().getCell(new int[][]{{3, 5}}).setFigure(new Pawn(Color.BLACK));
        from = new int[][]{{3, 3}};
        to = new int[][]{{3, 6}};
        String obstacleMove = "d4d7";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(obstacleMove, game));

        // Assert it eat opponent figures
        from = new int[][]{{3, 3}};
        to = new int[][]{{3, 5}};
        String eatMove = "d4d6";

        gameplayService.move(eatMove, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.R, game.getBoard().getCell(to).getFigure().getType());
    }

    @Test
    void moveBishop() {
        Game game = new Game();
        game.initialize();

        // Move the bishop diagonally
        int[][] from = new int[][]{{2, 0}};
        int[][] to = new int[][]{{0, 2}};
        // Remove the pawns from the way
        game.getBoard().getCell(new int[][]{{1, 1}}).setFigure(null);
        String move = "c1a3";

        gameplayService.move(move, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.B, game.getBoard().getCell(to).getFigure().getType());

        // Test invalid move (non-diagonal)
        String invalidMove = "a3a4";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(invalidMove, game));

        // Assert it doesn't go through obstacles
        game.getBoard().getCell(new int[][]{{3, 5}}).setFigure(new Pawn(Color.BLACK));
        from = new int[][]{{0, 2}};
        to = new int[][]{{4, 6}};
        String obstacleMove = "a3e7";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(obstacleMove, game));

        // Assert it eats opponent figures
        from = new int[][]{{0, 2}};
        to = new int[][]{{3, 5}};
        String eatMove = "a3d6";

        gameplayService.move(eatMove, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.B, game.getBoard().getCell(to).getFigure().getType());
    }

    @Test
    void moveKnight() {
        Game game = new Game();
        game.initialize();

        // Move the knight in an L-shape
        int[][] from = new int[][]{{1, 0}};
        int[][] to = new int[][]{{2, 2}};
        String move = "b1c3";

        gameplayService.move(move, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.N, game.getBoard().getCell(to).getFigure().getType());

        // Test invalid move
        from = new int[][]{{2, 2}};
        to = new int[][]{{3, 4}};
        String invalidMove = "c3e5";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(invalidMove, game));

        // Test eating opponent figure
        game.getBoard().getCell(new int[][]{{1, 4}}).setFigure(new Pawn(Color.BLACK));
        from = new int[][]{{2, 2}};
        to = new int[][]{{1, 4}};
        String eatMove = "c3b5";

        gameplayService.move(eatMove, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.N, game.getBoard().getCell(to).getFigure().getType());

        // Test not eating own figure
        game.getBoard().getCell(new int[][]{{0, 2}}).setFigure(new Pawn(Color.WHITE));
        from = new int[][]{{2, 2}};
        to = new int[][]{{0, 2}};
        String failEat = "b5a3";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(failEat, game));
    }

    @Test
    void moveKing() {
        Game game = new Game();
        game.initialize();

        // Move the king one step to the right
        int[][] from = new int[][]{{4, 0}};
        int[][] to = new int[][]{{5, 0}};
        // Remove the figure from the way
        game.getBoard().getCell(to).setFigure(null);
        String move = "e1f1";

        gameplayService.move(move, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.K, game.getBoard().getCell(to).getFigure().getType());

        // Test invalid move (greater than one step)
        from = new int[][]{{5, 0}};
        to = new int[][]{{5, 2}};
        // Remove the figure from the way
        game.getBoard().getCell(new int[][]{{5, 1}}).setFigure(null);
        String invalidMove = "f1f3";

        assertThrows(InvalidMoveException.class, () -> gameplayService.move(invalidMove, game));

        // Move 1 step
        move = "f1f2";

        gameplayService.move(move, game);

        // Test diagonal move
        from = new int[][]{{5, 1}};
        to = new int[][]{{6, 2}};
        String diagonalMove = "f2g3";

        gameplayService.move(diagonalMove, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.K, game.getBoard().getCell(to).getFigure().getType());

        // Test eating opponent figure
        game.getBoard().getCell(new int[][]{{7, 3}}).setFigure(new Rook(Color.BLACK));
        from = new int[][]{{6, 2}};
        to = new int[][]{{7, 3}};
        String eatMove = "g3h4";

        gameplayService.move(eatMove, game);

        assertNull(game.getBoard().getCell(from).getFigure());
        assertEquals(Type.K, game.getBoard().getCell(to).getFigure().getType());

        // Test it cant move under check
        // Spawn a pawn that checks the king
        Cell pawnCell = game.getBoard().getCell(new int[][]{{6, 5}});
        pawnCell.setFigure(new Pawn(Color.BLACK));
        game.getCurrentOpponent().addOccupiedCell(pawnCell);
        from = new int[][]{{7, 3}};
        to = new int[][]{{7, 4}};
        String failMove = "h4h5";

        assertThrows(CheckException.class, () -> gameplayService.move(failMove, game));
    }


}
