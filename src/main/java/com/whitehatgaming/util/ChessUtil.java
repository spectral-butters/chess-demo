package com.whitehatgaming.util;

import com.whitehatgaming.entity.Move;
import com.whitehatgaming.exceptions.InvalidMoveException;
import org.springframework.util.StringUtils;

import java.util.Arrays;

public class ChessUtil {
    public static final String MOVE_REGEXP = "^[a-h][1-8][a-h][1-8]$";

    public static Move createMove(String move) {
        if (!StringUtils.hasText(move) || !move.matches(MOVE_REGEXP)) {
            throw new InvalidMoveException("Invalid move format. Expected format: [from][to] (e.g. a2a4)");
        }

        // Extract the coordinates from the move string
        int fromX = Character.toLowerCase(move.charAt(0)) - 'a';
        int fromY = Character.getNumericValue(move.charAt(1)) - 1;
        int toX = Character.toLowerCase(move.charAt(2)) - 'a';
        int toY = Character.getNumericValue(move.charAt(3)) - 1;

        int [][] from = {{fromX, fromY}};
        int [][] to = {{toX, toY}};

        if (Arrays.deepEquals(from, to))
            throw new InvalidMoveException("Invalid move format. From and to coordinates are the same.");

        return new Move(from, to);
    }

}
