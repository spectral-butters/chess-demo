package com.whitehatgaming.exceptions;

public class CheckException extends InvalidMoveException {
    public CheckException(String message) {
        super(message);
    }

}
