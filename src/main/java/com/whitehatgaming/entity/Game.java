package com.whitehatgaming.entity;

import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Arrays;
import java.util.stream.Collectors;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Game {

    Board board;
    Player player1;
    Player player2;
    Player currentPlayer;
    boolean isFinished;

    public void initialize() {
        board = new Board();
        board.initialize();

        player1 = new Player("W", Color.WHITE);
        player1.setKingCell(board.getCells()[4][0]);
        player1.setOccupiedCells(Arrays.stream(board.getCells()).flatMap(Arrays::stream)
                .filter(cell -> cell.getFigure() != null
                && cell.getFigure().getColor() == Color.WHITE).collect(Collectors.toSet()));

        player2 = new Player("B", Color.BLACK);
        player2.setKingCell(board.getCells()[4][7]);
        player2.setOccupiedCells(Arrays.stream(board.getCells()).flatMap(Arrays::stream)
                .filter(cell -> cell.getFigure() != null
                        && cell.getFigure().getColor() == Color.BLACK).collect(Collectors.toSet()));

        currentPlayer = player1;
        isFinished = false;
    }

    public Player getCurrentOpponent() {
        return currentPlayer == player1 ? player2 : player1;
    }

    public void switchPlayer() {
        currentPlayer = currentPlayer == player1 ? player2 : player1;
    }
}
