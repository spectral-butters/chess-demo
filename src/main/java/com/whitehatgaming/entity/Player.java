package com.whitehatgaming.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;

@Data
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Player {

    final String name;
    final Color color;

    Cell kingCell;
    Set<Cell> occupiedCells = new HashSet<>();

    public void addOccupiedCell(Cell cell) {
        occupiedCells.add(cell);
    }

    public void removeOccupiedCell(Cell cell) {
        occupiedCells.remove(cell);
    }

}
