package com.whitehatgaming.entity;

import com.whitehatgaming.entity.figures.Figure;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.Objects;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Cell {

    final int x;
    final int y;
    Figure figure;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return x == cell.x && y == cell.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
