package com.whitehatgaming.entity;

import com.whitehatgaming.entity.figures.*;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class Board {

    Cell[][] cells;
    public static final int BOARD_SIZE = 8;

    public Cell getCell(int[][] coordinate) {
        return cells[coordinate[0][0]][coordinate[0][1]];
    }

    public void moveFigure(Move move) {
        Cell fromCell = getCell(move.getFrom());
        Cell toCell = getCell(move.getTo());
        toCell.setFigure(fromCell.getFigure());
        fromCell.setFigure(null);
    }

    public void initialize() {
        cells = new Cell[BOARD_SIZE][BOARD_SIZE];
        // Initialize empty board
        for (int row = 0; row < BOARD_SIZE; row++) {
            for (int col = 0; col < BOARD_SIZE; col++) {
                cells[row][col] = new Cell(row, col);
            }
        }

        // Initialize pawns
        for (int row = 0; row < BOARD_SIZE; row++) {
            cells[row][1].setFigure(new Pawn(Color.WHITE));
            cells[row][6].setFigure(new Pawn(Color.BLACK));
        }

        // Initialize rooks
        cells[0][0].setFigure(new Rook(Color.WHITE));
        cells[7][0].setFigure(new Rook(Color.WHITE));
        cells[0][7].setFigure(new Rook(Color.BLACK));
        cells[7][7].setFigure(new Rook(Color.BLACK));

        // Initialize knights
        cells[1][0].setFigure(new Knight(Color.WHITE));
        cells[6][0].setFigure(new Knight(Color.WHITE));
        cells[1][7].setFigure(new Knight(Color.BLACK));
        cells[6][7].setFigure(new Knight(Color.BLACK));

        // Initialize bishops
        cells[2][0].setFigure(new Bishop(Color.WHITE));
        cells[5][0].setFigure(new Bishop(Color.WHITE));
        cells[2][7].setFigure(new Bishop(Color.BLACK));
        cells[5][7].setFigure(new Bishop(Color.BLACK));

        // Initialize queens
        cells[3][0].setFigure(new Queen(Color.WHITE));
        cells[3][7].setFigure(new Queen(Color.BLACK));

        // Initialize kings
        cells[4][0].setFigure(new King(Color.WHITE));
        cells[4][7].setFigure(new King(Color.BLACK));

    }

    public void printBoard() {

        System.out.println(" + a  + b  + c  + d  + e  + f  + g  + h  ");
        System.out.println(" +--+--+--+--+--+--+--+--+--+--+--+--+--+--+");

        for (int y = BOARD_SIZE - 1; y >= 0; y--) {
            System.out.print((y + 1) + "|");
            for (int x = 0; x < BOARD_SIZE; x++) {
                Figure figure = cells[x][y].getFigure();
                if (figure != null) {
                    String figureSymbol = figure.getShortCode();
                    System.out.print(" " + figureSymbol + " ");
                } else {
                    System.out.print("    ");
                }
                System.out.print("|");
            }
            System.out.println(" " + (y + 1));
            System.out.println(" +--+--+--+--+--+--+--+--+--+--+--+--+--+--+");
        }

        System.out.println(" + a  + b  + c  + d  + e  + f  + g  + h  ");
    }

}
