package com.whitehatgaming.entity.figures;

import com.whitehatgaming.entity.*;

public interface Figure {
    boolean canMove(Board board, Player player, Move move);

    Color getColor();

    Type getType();

    String getShortCode();
}
