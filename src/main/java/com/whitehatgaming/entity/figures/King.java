package com.whitehatgaming.entity.figures;

import com.whitehatgaming.entity.*;
import com.whitehatgaming.exceptions.InvalidMoveException;

public class King extends AbstractFigure {

    public King(Color color) {
        super(Type.K, color);
    }

    @Override
    public boolean canMove(Board board, Player player, Move move) {
        super.canMove(board, player, move);

        int[][] from = move.getFrom();
        int[][] to = move.getTo();

        int fromX = from[0][0];
        int fromY = from[0][1];
        int toX = to[0][0];
        int toY = to[0][1];

        // Check that the king is moving one square in any direction
        int deltaX = Math.abs(toX - fromX);
        int deltaY = Math.abs(toY - fromY);

        if ((deltaX > 1 || deltaY > 1) || (deltaX == 0 && deltaY == 0)) {
            throw new InvalidMoveException("Invalid move for King");
        }

        return true;
    }
}

