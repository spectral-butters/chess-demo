package com.whitehatgaming.entity.figures;

import com.whitehatgaming.entity.*;
import com.whitehatgaming.exceptions.InvalidMoveException;

public class Knight extends AbstractFigure {
    public Knight(Color color) {
        super(Type.N, color);
    }

    @Override
    public boolean canMove(Board board, Player player, Move move) {
        super.canMove(board, player, move);

        int[][] from = move.getFrom();
        int[][] to = move.getTo();

        int fromX = from[0][0];
        int fromY = from[0][1];
        int toX = to[0][0];
        int toY = to[0][1];

        // Check if the knight is moving in an L-shaped pattern
        int deltaX = Math.abs(toX - fromX);
        int deltaY = Math.abs(toY - fromY);

        if ((deltaX == 2 && deltaY == 1) || (deltaX == 1 && deltaY == 2)) {
            return true;
        }

        throw new InvalidMoveException("Invalid move for Knight");
    }
}
