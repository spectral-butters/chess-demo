package com.whitehatgaming.entity.figures;

import com.whitehatgaming.entity.*;
import com.whitehatgaming.exceptions.InvalidMoveException;

public class Queen extends AbstractFigure {
    public Queen(Color color) {
        super(Type.Q, color);
    }

    @Override
    public boolean canMove(Board board, Player player, Move move) {
        super.canMove(board, player, move);

        int[][] from = move.getFrom();
        int[][] to = move.getTo();

        int fromX = from[0][0];
        int fromY = from[0][1];
        int toX = to[0][0];
        int toY = to[0][1];

        // If moving vertically or horizontally
        if (fromX == toX || fromY == toY) {
            return canMoveInline(board, move);
        }
        // If moving diagonally
        else if (Math.abs(toX - fromX) == Math.abs(toY - fromY)) {
            return canMoveDiagonally(board, move);
        }

        throw new InvalidMoveException("Invalid move for Queen");
    }

}
