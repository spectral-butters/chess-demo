package com.whitehatgaming.entity.figures;

import com.whitehatgaming.entity.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Bishop extends AbstractFigure {
    public Bishop(Color color) {
        super(Type.B, color);
    }

    @Override
    public boolean canMove(Board board, Player player, Move move) {
        super.canMove(board, player, move);

        return canMoveDiagonally(board, move);

    }
}
