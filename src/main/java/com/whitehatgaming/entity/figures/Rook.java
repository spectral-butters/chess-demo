package com.whitehatgaming.entity.figures;

import com.whitehatgaming.entity.*;
import lombok.Getter;

@Getter
public class Rook extends AbstractFigure {
    public Rook(Color color) {
        super(Type.R, color);
    }

    @Override
    public boolean canMove(Board board, Player player, Move move) {
        super.canMove(board, player, move);

        return canMoveInline(board, move);

    }

}
