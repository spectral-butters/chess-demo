package com.whitehatgaming.entity.figures;

import com.whitehatgaming.entity.*;
import com.whitehatgaming.exceptions.InvalidMoveException;

import static com.whitehatgaming.entity.Color.WHITE;

public class Pawn extends AbstractFigure {

    public Pawn(Color color) {
        super(Type.P, color);
    }

    @Override
    public boolean canMove(Board board, Player player, Move move) {
        super.canMove(board, player, move);

        int[][] from = move.getFrom();
        int[][] to = move.getTo();

        int fromX = from[0][0];
        int fromY = from[0][1];
        int toX = to[0][0];
        int toY = to[0][1];

        // Determine the direction based on the pawn's color
        int direction = (getColor() == WHITE) ? 1 : -1;

        // Pawn is moving forward by one square
        if (toX == fromX && toY == fromY + direction && board.getCell(to).getFigure() == null) {
            return true;
        }

        // Pawn is moving forward by two squares on its first move
        if (toX == fromX
                && (WHITE == getColor() ? fromY == 1 : fromY == 6)
                && toY == fromY + (2 * direction)
                && board.getCell(to).getFigure() == null) {
            return true;
        }

        // Check if the pawn is capturing diagonally
        if (Math.abs(toX - fromX) == 1 && toY == fromY + direction) {
            Figure targetFigure = board.getCell(to).getFigure();
            if (targetFigure != null) {
                return true;
            }
        }

        // None of the conditions are met
        throw new InvalidMoveException("Invalid move for Pawn");
    }

}
