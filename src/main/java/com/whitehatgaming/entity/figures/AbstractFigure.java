package com.whitehatgaming.entity.figures;

import com.whitehatgaming.entity.*;
import com.whitehatgaming.exceptions.InvalidMoveException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public abstract class AbstractFigure implements Figure {

    Type type;
    Color color;

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public Color getColor() {
        return color;
    }

    public boolean canMove(Board board, Player player, Move move) {

        // Check if the source square is not empty
        Figure sourceAbstractFigure = board.getCell(move.getFrom()).getFigure();
        if (sourceAbstractFigure == null) {
            throw new InvalidMoveException("Source cell is empty");
        }

        // Check if the source cell is occupied by the player's own figure
        if (sourceAbstractFigure.getColor() != player.getColor()) {
            throw new InvalidMoveException("Figure is wrong color");
        }

        // Check if the target cell is not occupied by the player's own figure
        Figure targetAbstractFigure = board.getCell(move.getTo()).getFigure();
        if (targetAbstractFigure != null && targetAbstractFigure.getColor() == getColor()) {
            throw new InvalidMoveException("Target cell is occupied");
        }

        return true;
    }

    public boolean canMoveDiagonally(Board board, Move move) {
        int[][] from = move.getFrom();
        int[][] to = move.getTo();

        int fromX = from[0][0];
        int fromY = from[0][1];
        int toX = to[0][0];
        int toY = to[0][1];

        // Check that figure is moving diagonally
        if (Math.abs(toX - fromX) != Math.abs(toY - fromY)) {
            throw new InvalidMoveException("Invalid move");
        }

        int xDirection = toX > fromX ? 1 : -1;
        int yDirection = toY > fromY ? 1 : -1;

        int currentX = fromX + xDirection;
        int currentY = fromY + yDirection;

        while (currentX != toX && currentY != toY) {
            int [][] current = new int[][]{{currentX, currentY}};
            if (board.getCell(current).getFigure() != null) {
                throw new InvalidMoveException("Obstacle: Invalid move for " + type.getName());
            }
            currentX += xDirection;
            currentY += yDirection;
        }

        return true;
    }

    public boolean canMoveInline(Board board, Move move) {
        int[][] from = move.getFrom();
        int[][] to = move.getTo();

        int fromX = from[0][0];
        int fromY = from[0][1];
        int toX = to[0][0];
        int toY = to[0][1];

        // Check if figure is moving horizontally or vertically
        if (fromX != toX && fromY != toY
                || fromX == toX && fromY == toY) {
            throw new InvalidMoveException("Invalid move for " + type.getName());
        }

        // Check if there are any obstructing figures on the horizontal or vertical path
        if (fromX == toX) {
            // Moving horizontally
            int deltaY = (toY > fromY) ? 1 : -1;

            int currentY = fromY + deltaY;

            while (currentY != toY) {
                int [][] current = new int[][]{{fromX, currentY}};
                if (board.getCell(current).getFigure() != null) {
                    throw new InvalidMoveException("Obstacle: Invalid move for " + type.getName());
                }
                currentY += deltaY;
            }
        }
        else {
            // Moving vertically
            int deltaX = (toX > fromX) ? 1 : -1;

            int currentX = fromX + deltaX;

            while (currentX != toX) {
                int [][] current = new int[][]{{currentX, fromY}};
                if (board.getCell(current).getFigure() != null) {
                    throw new InvalidMoveException("Obstacle: Invalid move for " + type.getName());
                }
                currentX += deltaX;
            }
        }

        return true;
    }

    @Override
    public String getShortCode() {
        return color.name().substring(0, 1) + type.name();
    }
}
