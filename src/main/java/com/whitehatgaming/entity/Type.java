package com.whitehatgaming.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public enum Type {
    R("Rook"),
    N("Knight"),
    B("Bishop"),
    K("King"),
    Q("Queen"),
    P("Pawn");

    String name;
}
