package com.whitehatgaming.service;

import com.whitehatgaming.entity.*;
import com.whitehatgaming.exceptions.CheckException;
import com.whitehatgaming.exceptions.InvalidMoveException;
import com.whitehatgaming.entity.figures.Figure;
import com.whitehatgaming.util.ChessUtil;
import com.whitehatgaming.util.Command;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Scanner;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class GameplayService {

    ApplicationShutdown applicationShutdown;

    @PostConstruct
    void init() {
        StringBuilder builder = new StringBuilder()
                .append("Welcome to the Chess game!\n")
                .append("To move a figure type `from` and `to` cells (e.g. a2a4)\n")
                .append("To quit the game type 'quit'\n")
                .append("To restart the game type 'restart'\n")
                .append("And may the best be the winner!'\n");
        System.out.println(builder);
        launchGame();
    }

    /**
     * Entry point for the application
     * App will be running until user types 'quit'
     * Every player makes a move and switches the turn
     * Input is read from console and routed to the corresponding command
     * Unknown command is treated as a move
     */
    public void launchGame() {
        Scanner scanner = new Scanner(System.in);

        Game game = new Game();
        game.initialize();
        // Display initial board
        game.getBoard().printBoard();
        System.out.println("To quit the game type 'quit'");
        System.out.println("To restart the game type 'restart'");
        System.out.println(game.getCurrentPlayer().getColor() + " turn");

        while (!game.isFinished()) {
            // Get input from console
            String input = scanner.nextLine();
            // Trigger the command
            try {
                route(input, game);
            } catch (InvalidMoveException e) {
                System.out.println(e.getMessage());
                continue;
            } catch (Exception e) {
                System.out.println("Internal error: " + e.getMessage());
                continue;
            }
            // If the move is successful - switch the player
            // Print the board
            game.getBoard().printBoard();
            // Switch player
            game.switchPlayer();
            System.out.println(game.getCurrentPlayer().getColor() + " turn");
        }
    }

    /**
     * Main function of the gameplay
     * We parse input into Move object and validate it
     * If the move is valid, we validate that the player will not be under check after the move
     * If the move is valid, figures are moved and player state is updated
     * If the move is invalid, exception is thrown and player is prompted to enter another move
     * @param input - user input from console
     * @param game - current game
     */
    public void move(String input, Game game) {

        // Build the move with coordinates
        Move move = ChessUtil.createMove(input);

        // Validate if move is possible for the figure
        Cell fromCell = game.getBoard().getCell(move.getFrom());
        Figure figure = fromCell.getFigure();
        if (figure == null) {
            throw new InvalidMoveException("No figure found on the source cell");
        }
        figure.canMove(game.getBoard(), game.getCurrentPlayer(), move);

        // Validate the move does not cause check
        if (isUnderCheck(game, move)) {
            throw new CheckException("Move causes check");
        }

        Cell toCell = game.getBoard().getCell(move.getTo());
        boolean isOccupied = game.getBoard().getCell(move.getTo()).getFigure() != null;
        // If the target cell is occupied, remove the figure from the opponent's occupied cells
        if (isOccupied) {
            if (Type.K == toCell.getFigure().getType()) {
                throw new InvalidMoveException("Cannot attack King");
            }
            game.getCurrentOpponent().removeOccupiedCell(toCell);
        }
        game.getCurrentPlayer().addOccupiedCell(toCell);
        game.getCurrentPlayer().removeOccupiedCell(fromCell);
        // If King is moved, update the King cell
        if (Type.K == figure.getType()) {
            game.getCurrentPlayer().setKingCell(game.getBoard().getCell(move.getTo()));
        }
        // Move the figure on the board
        game.getBoard().moveFigure(move);

    }

    private boolean isUnderCheck(Game game, Move move) {
        Board board = game.getBoard();

        Figure figureFrom = board.getCell(move.getFrom()).getFigure();
        Figure figureTo = board.getCell(move.getTo()).getFigure();

        // Imitate the move on the board
        board.moveFigure(move);

        // Get the current player's king position
        // If King is moved, pass new king position
        // Or else get the king position from the player's king cell
        Cell kingCell = Type.K == figureFrom.getType()
                ? board.getCell(move.getTo())
                : game.getCurrentPlayer().getKingCell();

        // Check if the player's king is under attack after move
        boolean underCheck = isUnderCheck(board, kingCell, game);
        // Revert the move
        board.getCell(move.getFrom()).setFigure(figureFrom);
        board.getCell(move.getTo()).setFigure(figureTo);
        return underCheck;
    }

    private boolean isUnderCheck(Board board, Cell kingCell, Game game) {
        // Iterate through all opponent figures and check if any of them can attack the target position
        for (Cell cell : game.getCurrentOpponent().getOccupiedCells()) {
            Figure figure = cell.getFigure();
            Player opponent = game.getCurrentOpponent();

            int[][] from = new int[][]{{cell.getX(), cell.getY()}};
            int[][] to = new int[][]{{kingCell.getX(), kingCell.getY()}};
            // Imitate opponent move
            Move attackMove = new Move(from, to);
            try {
                figure.canMove(board, opponent, attackMove);
                return true;
            } catch (InvalidMoveException e) {
                // Opponent figure cannot attack King cell
            }
        }

        return false; // Target position is not under attack
    }

    private void route(String input, Game game) {
        Command command;
        try {
            command = Command.valueOf(input.toUpperCase());
        } catch (IllegalArgumentException e) {
            move(input, game);
            return;
        }

        switch (command) {
            case QUIT:
                System.out.println("Well played!");
                game.setFinished(true);
                applicationShutdown.shutdown();
                break;
            case RESTART:
                launchGame();
                break;
        }
    }
}
