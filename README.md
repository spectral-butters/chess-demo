# Chess Game

Welcome to the Chess Game project!

This project aims to provide a basic implementation of a chess game with move validation and check detection.

It includes a service for handling game mechanics and a test suite to ensure the correctness of the implemented logic.

Application is console based, using `Java 16, Spring boot and Maven`.

## Installation
1. Clone the repository
2. Run `mvn clean install` to run tests and build the project
3. Run `mvn spring-boot:run` to start the application
4. Follow the instructions on the console to play the game
  Use `quit` to exit the game  
  Use `restart` to restart the game  
  Move prompt format: `a2a4` (from to)